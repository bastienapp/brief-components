import "./Contact.css";
import iconConnected from "../assets/green-circle.png";
import iconDisconnected from "../assets/red-circle.png";

// composant fonctionnel
function Contact(props) {
  /*
  props : {
    name: "Brenda Dupont",
    picture: "https://www.fakepersongenerator.com/Face/male/male2016108394376874.jpg"
    status: true
  }
  */

  return (
    <article className="Contact">
      <h3>{ props.name }</h3>
      <div className="profile-picture__container">
        <img className="profile-picture" src={ props.picture } alt={ "Photo de " + props.name } />
        {
        props.status
          ? <img className="status-icon" src={iconConnected} alt="Connected" />
          : <img className="status-icon" src={iconDisconnected} alt="Disconnected" />
        }
      </div>
    </article>
  );
}

export default Contact;