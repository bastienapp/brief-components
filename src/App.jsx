import './App.css'
import Contact from './components/Contact';

function App() {

  return (
    <>
      <Contact
        name="Michel Durand"
        picture="https://www.fakepersongenerator.com/Face/male/male2016108394376874.jpg"
        status={true}
      />
    </>
  )
}

export default App
