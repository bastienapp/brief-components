# React - Composants et props

Apprend à créer des composants et à utiliser les props avec React

## Ressources

- [Your First Component](https://react.dev/learn/your-first-component)
- [Passing Props to a Component](https://react.dev/learn/passing-props-to-a-component)
- [Conditional Rendering - Conditional ternary operator](https://react.dev/learn/conditional-rendering#conditional-ternary-operator--)

## Contexte du projet

Ce brief va te proposer de créer un composant, qui va représenter un contact dans une application de messagerie :

![Contact](https://i.imgur.com/s8hmly5.png)
​
## Modalités pédagogiques

​Crée un dossier `components` dans le dossier `src` de ton projet.

Dans le dossier `components`, crée un nouveau composant fonctionnel `Contact.jsx`.

​Tu trouveras des informations sur la création d'un composant fonctionnel ici : [Your First Component](https://react.dev/learn/your-first-component).

Les valeurs du nom, de l'image de profil et du statut connecté (représenté par le rond vert, ou rouge) devront provenir du composant parent `App.jsx`.

Tu trouveras des informations sur les **props** ici : [Passing Props to a Component](https://react.dev/learn/passing-props-to-a-component).​

Tu trouveras des informations sur l'affichage conditionnel ici (qui utilise un opérateur ternaire) : [Conditional Rendering - Conditional ternary operator](https://react.dev/learn/conditional-rendering#conditional-ternary-operator--).​

Dans le composant `App`, appelle plusieurs fois ton composant `Contact`, avec des valeurs de props différentes, afin de tester que tout fonctionne correctement.

## Modalités d'évaluation

- Un dépôt GitLab contient le code du projet
- Un composant fonctionnel "Contact" affiche un contact de messagerie
- Les informations du contact proviennent des props
- En fonction de l'état "connecté" ou "non connecté", la couleur de la pastille de connexion change
- Le composant "Contact" et appelé plusieurs fois dans le composant "App" (avec des valeurs de props différentes)

## Livrables

- Un lien vers un dépôt GitLab

## Critères de performance

- L’interface est conforme au dossier de conception
- La charte graphique est respecté
- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions